/* 
Теоретичні питання
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?
   Можна визначити, яку саме клавішу клавіатури натиснув користувач, використовуючи властивості об'єкта події event. Для цього є кілька варіантів, основними з яких є event.code та event.key.

2. Яка різниця між event.code() та event.key()?
   Основна різниця між ними полягає в тому, що event.code повертає стандартизований код клавіші, тоді як event.key повертає фактичний символ, який надрукувано на клавіші.

3. Які три події клавіатури існує та яка між ними відмінність?
   keydown, keyup, keypress



Практичне завдання.
Реалізувати функцію підсвічування клавіш.

Технічні вимоги:

- У файлі index.html лежить розмітка для кнопок.
- Кожна кнопка містить назву клавіші на клавіатурі
- Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.
Але якщо при натискані на кнопку її  не існує в розмітці, то попередня активна кнопка повина стати неактивною.
*/

document.addEventListener('keydown', function(event) {
    let keyCode = event.code;
    let buttons = document.querySelectorAll('.btn');

    buttons.forEach(function(button) {
        if (keyCode === 'Enter' && button.textContent === 'Enter') {
            toggleButtonColor(button);
        } else if (keyCode === 'Tab' && button.textContent === 'Tab') {
            toggleButtonColor(button);
        } else if (button.textContent === keyCode.substring(3)) {
            toggleButtonColor(button);
        } else {
            button.style.backgroundColor = 'black';
        }
    });
});

function toggleButtonColor(button) {
    if (button.style.backgroundColor === 'blue') {
        button.style.backgroundColor = 'black';
    } else {
        button.style.backgroundColor = 'blue';
    }
}








 


